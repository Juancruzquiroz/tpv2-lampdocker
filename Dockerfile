FROM ubuntu:latest

# Info acerca de la imagen
LABEL maintainer="Juan De La Cruz Quiroz"
LABEL version="1.0"
LABEL description="LAMP Nginx dockerizado"

# Pasamos el argumento no interactivo
ARG DEBIAN_FRONTEND=noninteractive

# Instalación de paquetes
RUN apt update && apt install -y apache2 php libapache2-mod-php && apt clean && apt autoremove
 
# Variables de entorno
ENV APACHE_SERVER_NAME default
 
# Puntos de montaje
VOLUME ["/var/www/html"]
 
# Crear Directorio 'app'
RUN mkdir -p /app
 
# Copiar archivos al directorio 'app'
COPY index.php /app
 
# Copiar default vhost al contenedor
COPY 000-default.conf /etc/apache2/sites-available/000-default.conf
 
# Copiar script de inicio al directorio raiz y dar permisos de ejecución
COPY entrypoint.sh /
RUN chmod +x /entrypoint.sh
 
# Exponer puertos
EXPOSE 80
 
# Comando a ejecutarse cuando se crea el contenedor
ENTRYPOINT ["/entrypoint.sh"]
CMD ["apachectl", "-D", "FOREGROUND"]
