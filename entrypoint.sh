#!/bin/bash
set -e

# Limpiar Contenido de '/var/www/html'
rm /var/www/html/index.html

# Copiar nuestro index.php a la carpeta del sitio web 
cp /app/index.php /var/www/html

# Pasaje de parametro Server Name 
sed -i 's/default/'"$APACHE_SERVER_NAME"'/' /etc/apache2/sites-available/000-default.conf 
 
 
# Iniciar servicio Apache
 
apachectl -D FOREGROUND
 
exec "$@"
