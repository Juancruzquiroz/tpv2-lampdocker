# TPv2-LampDocker

TP Proyecto de Instalación de Docker LAMP para sitios web estáticos utilizando Dockerfile
                                ACTUALIZACION DE TECNOLOGIA
                                                                                    Juan De La Cruz Quiroz
                                DESPLEGAR SERVIDOR LAMP DOKERIZADO
Preparando nuestro ambiente de trabajo
-Crear Directorio de Trabajo
    $ mkdir lampdocker

-Posicionarse dentro del directorio de trabajo
    $ cd lampdocker/

-Descargar los archivos del proyecto https://gitlab.com/Juancruzquiroz/tpv2-lampdocker
    -	Dockerfile
    -	entrypoint.sh
    -	index.php
    -	000-default.conf
    -	Readme

Manos a la obra
-Generar el contenedor Proxy Inverso NGinx
    $ docker run -d -p 80:80 -v /var/run/docker.sock:/tmp/docker.sock:ro jwilder/nginx-proxy

Verificamos que este corriendo el contenedor Proxy con el comando
    $ docker ps

-Crea nuestra Imagen Docker utilizando Dockerfile
    $ docker build –t lampdocker  (Indicar path donde se encuentra el archivo Dockerfile)
    *En mi caso la ruta de acceso al Dockerfile es /home/jcq/lampdocker/

-Verificamos que se creó la imagen con el comando
$ docker images

-Creamos nuestro Contenedor a través de la imagen generada
        $.docker run -d --name miweblamp \
		-e APPSERVERNAME=tp.weblamp.istea \
		-e APPALIAS=www.miweblamp.istea \
		-e VIRTUAL_HOST=weblamp.istea \
		-v /var/www/html:/var/www/html/ \
		-p 85:80 \
		lampdocker

-Verificamos que este corriendo el contenedor Proxy con el comando
    $ docker ps

-Modificamos el archivo hosts de nuestro equipo para que pueda acceder a nuestra página web
*Path para Windows (editar con privilegios de administrador)
    C:/windows/sistem32/drivers/etc/hosts
*Path para Linux (editar con privilegios de root)
    $ sudo vim /etc/hosts
Se debe agregar la IP del equipo donde está corriendo el Contenedor Docker y guardar los cambios

-Para acceder a nuestra web
Abrimos un navegador web colocamos la IP del equipo donde está corriendo el contenedor indicando el puerto asignado cuando generamos nuestro contenedor. Ejemplo: http://xxx.xxx.xxx:85 (las x representan los octetos de la IP a utilizar)
